var finalLatitude, finalLongitude;
var currentPosition, currentDate;

window.onload = () => {

    var today = new Date();
    currentDate = today;


    navigator.geolocation.getCurrentPosition(function (position) {
        currentPosition = position.coords;
        finalLatitude = position.coords.latitude;
        finalLongitude = position.coords.longitude;
        check();
        return renderBox(finalLatitude, finalLongitude);
    });
        };

function createNew(){
    alert("You are new here, let me just save your location!");
    localStorage.setItem('coords', currentPosition);
    localStorage.setItem('coordsLat', currentPosition.latitude);
    localStorage.setItem('coordsLon', currentPosition.longitude);
    localStorage.setItem('TimeStamp', currentDate);
    localStorage.setItem('date', currentDate.getDate());
    localStorage.setItem('hour', currentDate.getHours());
}

function check(){
    if (localStorage.getItem('coords') == null){
        alert("Data doesn't exist");
        createNew();
        return;
    }else{
        var distance = measureDist(currentPosition.latitude, currentPosition.longitude, localStorage.getItem('coordsLat'), localStorage.getItem('coordsLon'));
        if (distance > 1000){
            alert("Too far!");
            createNew();
            return;
        }

        if (currentDate.getDate() - localStorage.getItem('date') > 0){
            if (currentDate.getHours() - localStorage.getItem('hour') > 0){
                alert("Expired");
                createNew();
                return;
            }
        }
        if(currentDate.getDate() - localStorage.getItem('date') > 1){
             alert("Expired");
                createNew();
                return;
        }
        finalLatitude = localStorage.getItem('coordsLat');
        finalLongitude = localStorage.getItem('coordsLon');
    }
    alert('A box already exists at ' + finalLatitude +' '+ finalLongitude + ' since ' + localStorage.getItem('TimeStamp'));
}


function renderBox(lat, long) {
    let scene = document.querySelector('a-scene');

    let latitude = lat;
    let longitude = long;

        // add place name
    let text = document.querySelector('a-entity');
    text.setAttribute('gps-entity-place', `latitude: ${latitude}; longitude: ${longitude};`);
    text.setAttribute('position', '0 0 -200');
    text.setAttribute('visible', true);
    text.setAttribute('scale', '0.05 0.05 0.05');

    text.addEventListener('loaded', () => {
        window.dispatchEvent(new CustomEvent('gps-entity-place-loaded', { detail: { component: this.el }}))
    });
}



function measureDist(lat1, lon1, lat2, lon2){  // generally used geo measurement function
    var R = 6378.137; // Radius of earth in KM
    var dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
    var dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
    Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    console.log(d*1000 + 'meters');
    return d * 1000; // meters
}